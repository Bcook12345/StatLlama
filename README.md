# StatLlama

Hi, welcome to the Stat Llama Repo! This README is a basic overview of Stat Llama and my drive to build it.

## Overview
Stat Llama is a Discord bot written in Python to gather player stats and display them in chat on command. That was the starting purpose but it may evolve to have more features as it grows.

## Are you aware of these bugs?
Right now, we are aware of the following **major** bugs.

- Challenges aren't working right now. The API we have right now, the developer has not found anything about Challenges in the current season.
- Fetching Player Stats doesn't work right now. We are currently working to fix this.

## Supported Games
Right now, we support Fortnite only. If you would like another game to be added please make a feature request.

## Determination
The reason why I want to build this bot is because I wanted to learn Python and I developed Discord bots in C# before so I thought this would be a good start because I'm not diving into an unknown API. It also seems when a project gets hard, I quit it. I'd like this to be my first project where I expand out of Beta and have a Public version. If you'd like to contribute, please read our Contributing Guidelines. This project is licensed under the MIT License.

## Special thanks!
I would personally like to thank on behalf of the community:
- KarkaLT for the Python wrapper and the patch of the News command.
- Sam for the founding and upkeep of FortniteAPI.com
- The FortniteAPI.com Discord Server for help getting started.
