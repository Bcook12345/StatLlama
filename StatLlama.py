import random
import asyncio
import aiohttp
import json
import time
import FortniteAPI
from discord.ext import commands
from discord import Game
from discord.ext.commands import Bot
from FortniteAPI import Stats
from FortniteAPI import Status
from FortniteAPI import Shop
from FortniteAPI import Challenges
from FortniteAPI import News
from FortniteAPI import User
from FortniteAPI import Upcoming

BOT_PREFIX = (".")
TOKEN = "InsertYourSecretTokenHere" 

client = Bot(command_prefix=BOT_PREFIX)

@client.command()
async def upcomingitems(brief='Items coming out soon', description='Finds items that are coming to an Item Shop near **you**!'):
    upcoming = Upcoming()
    items = upcoming.get_items()
    for item in items:
        await client.say(item.name)
        await client.say(item.price + "V-Bucks")
        await client.say(item.type)
        await client.say("||" + item.information + "||")

@client.command(brief='Ninja Stats', description='Finds out what Ninjas User ID and Devices are.')
async def ninja():
    stats = Stats("4735ce9132924caf8a5b17789b40f79c") # User id
    await client.say(stats.user_id) # Prints user id
    await client.say(stats.username) # Prints username
    await client.say(stats.devices) # Prints available user devices

@client.command(pass_context=True, brief='Gets User ID', description='Fetches the User ID and returns it to you.')
async def getuserid(ctx, player: str):
    user = User(player)
    await client.say("The user " + user.username + "'s ID is: " + user.id + ".")

#@client.command(pass_context=True)
async def kbmplayerstats(ctx, userid):
    stats = Stats(userid)
    await client.say(stats.username + " - **OVERALL:**")
    await client.say("Overall Wins: " + stats.keyboard.overall.wins)
    await asyncio.sleep(0.2)
    await client.say("Overall KD: " + stats.keyboard.overall.winrate)
    await asyncio.sleep(3)
    await client.say("**Solo:**")
    await client.say("Solo Wins: " + stats.keyboard.solo.wins)
    await asyncio.sleep(0.2)
    await client.say("Solo KD: " + stats.keyboard.solo.winrate)
    await asyncio.sleep(3)
    await client.say("**Duo**")
    await client.say("Duo Wins: " + stats.keyboard.duo.wins)
    await asyncio.sleep(0.2)
    await client.say("Duo KD: " + stats.keyboard.duo.winrate)
    await asyncio.sleep(3)
    await client.say("**Squads:**")
    await client.say("Squad Wins: " + stats.keyboard.squad.wins)
    await asyncio.sleep(0.2)
    await client.say("Squad KD: " + stats.keyboard.squad.winrate)

@client.command(brief='Discord link', description='This is the support server link.')
async def discord():
    await client.say("https://discord.gg/AmY7Hkk")

@client.command()
async def fnstatus(brief='Gets the status of the Fortnite Servers.', description='Gets the status of the Fortnite Servers.'):
    status = Status()
    await client.say("**"+status.message+"**" + " Fortnite is currently running **v" + status.version +"**" + " and the servers have been up for " + "**"+status.formatted+"**.")

@client.command(brief='Gets the Item Shop of Fortnite for today.', description='Gets the Item Shop of Fortnite for today.')
async def dailyshop():
    shop = Shop()
    print("Grabbing the daily shop items!")
    await client.say("Today's date is: " + shop.date)
    await asyncio.sleep(0.5)
    items = shop.get_items()
    for item in items:
        await client.say(item.name+ ", " + item.price + " V-Bucks")
        await asyncio.sleep(0.5)

@client.command()
async def dailyshopv2(brief='Gets the Item Shop of Fortnite for today.', description='Gets the Item Shop of Fortnite for today.'):
    shop = Shop()
    print("Grabbing the item shop using the new and improved command!")
    await client.say("Today's date is: " + shop.date + ".")
    items = shop.get_items()
    for item in items:
        await client.say(item.information)

#@client.command()
#async def challenges():
#    base = Challenges(7) # Season number
#    challenges = base.get_challenges(5) # Week number
#    print(base.season) # Prints season
#    print(base.current_week) # Prints current season week
#    print(base.star) # Prints link of a battle star image
#    for challenge in challenges:
#        print(challenge.id) # Prints challenge identifier
#        print(challenge.challenge) # Prints challenge
#        print(challenge.count) # Prints challenge amount
#        print(challenge.stars) # Prints amount of rewarded battle star
#        print(challenge.difficulty) # Prints challenge difficulty

#@client.command()
async def fnnews(brief='Gets the Fortnite News', description='Gets the Fortnite News'):
    print("Grabbing the Fortnite News!")
    newsInfo = News()
    news = newsInfo.get_news()
    for new in news:
        await client.say(new.title + new.body + "||" + new.image + "||")
        await client.say("-----")


@client.event
async def on_ready():
    await client.change_presence(game=Game(name="for: .help", type = 3))
    print("Logged in as " + client.user.name)



async def list_servers():
    await client.wait_until_ready()
    while not client.is_closed:
        print("Current servers:")
        for server in client.servers:
            print(server.name)
        await asyncio.sleep(600)


client.loop.create_task(list_servers())
client.run(TOKEN)
